<?php
$base='../../include/';

include $base."header.php";
require_once $base."conn.php";
?>
<div class="container">
    <?php
$sql = "SELECT id, address, country, user_id FROM address";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    
    echo "<table>
    <thead>
    <tr>
    <th>Id</th>
    <th>Address</th>
    <th>Country</th>
    <th>User Id</th>
    <th>Action</th>
    </tr>
    </thead>";
    echo "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
        
        echo"<tr>";
        echo "<td>". $row["id"]."</td>";
        echo "<td>". $row["address"]."</td>";
        echo "<td>". $row["country"]."</td>";
        echo "<td>". $row["user_id"]."</td>";
        
        echo '<td><a href="edit.php?id=' . $row['std_id'] . '">Edit</a> <a href="delete.php?id=' . $row['std_id'] . '"onclick="return confirmation()">Delete</a></td>';


        echo"</tr>";
       
       
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}

mysqli_close($conn);
?>
<script type="text/javascript">
    function confirmation() {
      return confirm('Are you sure you want to delete this?');
    }
</script>

<a href="address.php" button type="button" class="btn btn-primary">Back</button>

</div>