<?php
$base='../../include/';

include $base."header.php";
require_once $base."conn.php";
?>
<div class="container">
    <?php
$sql = "SELECT id, dvd_name, description, rented_status, created_date, rented_date, modified_date, return_date, user_id FROM dvd";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    
    echo "<table>
    <thead>
    <tr>
    <th>Id</th>
    <th>DVD name</th>
    <th>Description</th>
    <th>Rented Status</th>
    <th>Created Date</th>
    <th>Rented Date</th>
    <th>Modified Date</th>
    <th>Return Date</th>
    <th>User Id</th>
    </tr>
    </thead>";
    echo "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
        
        echo"<tr>";
        echo "<td>". $row["id"]."</td>";
        echo "<td>". $row["dvd_name"]."</td>";
        echo "<td>". $row["description"]."</td>";
        echo "<td>". $row["rented_status"]."</td>";
        echo "<td>". $row["created_date"]."</td>";
        echo "<td>". $row["rented_date"]."</td>";
        echo "<td>". $row["modified_date"]."</td>";
        echo "<td>". $row["return_date"]."</td>";
        echo "<td>". $row["user_id"]."</td>";
        echo '<td><a href="deletedvd.php?id=' . $row['id'] . '"onclick="return confirmation()">Delete</a></td>';


        echo"</tr>";
       
       
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}

mysqli_close($conn);
?>
<script type="text/javascript">
    function confirmation() {
      return confirm('Are you sure you want to delete this?');
    }
</script>

<a href="dvd.php" button type="button" class="btn btn-primary">Back</button>

</div>